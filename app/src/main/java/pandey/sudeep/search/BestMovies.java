package pandey.sudeep.search;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName="bestmovies")
public class BestMovies {

    @PrimaryKey
    private long id;
    private String movieName;
    private String genre;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public BestMovies(){

    }
    public BestMovies(int _id, String movie, String _genre){
        this.id=_id;
        this.movieName=movie;
        this.genre=_genre;
    }
}

