package pandey.sudeep.search;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private AppDatabase database;
    private Context context = this;
    private MovieAdapter adapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);




        final AppExecutors executors = new AppExecutors();
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                database = AppDatabase.getInstance(context);
            }
        });

        Button basicLoad = (Button)findViewById(R.id.button1);
        basicLoad.setOnClickListener(new View.OnClickListener() {

            /**
             * need to use AsyncTask for the below implementation.
             * However, it does seem that 'adapter' variable is
             * properly instantiated in different thread before the
             * recyclerview object gets the adapter in UI thread.
             */

            @Override
            public void onClick(View v) {
                executors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new MovieAdapter(AppDatabase.getInstance(context).getMovieDAO().loadAllMovies());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView = findViewById(R.id.recyclerView);
                                recyclerView.setHasFixedSize(true);
                                linearLayoutManager = new LinearLayoutManager(context);
                                recyclerView.setLayoutManager(linearLayoutManager);
                                recyclerView.setAdapter(adapter);
                            }
                        });

                    }
                });
            }
        });

    }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.options_menu, menu);

            // Associate searchable configuration with the SearchView
            SearchManager searchManager =
                    (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView =
                    (SearchView) menu.findItem(R.id.search).getActionView();
            searchView.setSearchableInfo(
                    searchManager.getSearchableInfo(getComponentName()));

            return true;
        }

    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.search:
                //implementation
                return true;

            default:
                // implementation
                return super.onOptionsItemSelected(item);

        }
    }*/
}
