package pandey.sudeep.search;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;

import java.util.List;

public class SearchableActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MovieAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);

        handleIntent(getIntent());

    }

    @Override
    protected void onNewIntent(Intent intent) {

        handleIntent(intent);
    }



    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
            // SearchManager.QUERY is the key that a SearchManager will use to send a query string
            // to an Activity.
            String query = getIntent().getStringExtra(SearchManager.QUERY);

            doMySearch(query);
        }
    }

    private void doMySearch(final String query){

        new AppExecutors().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                final List<BestMovies> movies = AppDatabase.getInstance(context).getMovieDAO().loadMoviesForGenre(query);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerView = findViewById(R.id.recyclerView);
                        recyclerView.setHasFixedSize(true);
                        linearLayoutManager=new LinearLayoutManager(context);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        adapter = new MovieAdapter(movies);
                        recyclerView.setAdapter(adapter);
                    }
                });
            }
        });
    }
}
